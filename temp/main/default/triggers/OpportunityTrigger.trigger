/**
 * @File Name          : OpportunityTrigger.trigger
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/11/2020, 3:57:38 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/9/2020   acantero     Initial Version
**/
trigger OpportunityTrigger on Opportunity (before insert) {
    if (Trigger.isBefore) {
        if (Trigger.isInsert) {
            OpportunityTriggerUtility.beforeInsert(Trigger.new);
        }
    }

}