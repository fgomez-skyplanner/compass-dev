/**
 * Test class for: QuickbookProductSearchCtrl
 * @author fernando Gomez, SkyPlanner LLC
 */
@isTest
private class QbExceptionsTest {
	/**
	 * Test classes
	 */
	@isTest
	static void testClasses() {
		QbExceptions.BaseException b1, b2, b3;

		b1 = new QbExceptions.AuthException();
		b2 = new QbExceptions.HttpException();
		b3 = new QbExceptions.BadDataException();

		System.assertEquals(401, b1.getStatusCode());
		System.assertEquals(500, b2.getStatusCode());
		System.assertEquals(400, b3.getStatusCode());
	}
}
