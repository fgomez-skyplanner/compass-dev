/**
 * Sends requests to QuickBooks. Generic requests.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 6/18/2020
 */
public class QbHttp {
	private final String CONTENT_TYPE = 'application/json';
	
	private String baseUrl;

	/**
	 * @param credentialsName this is required to
	 * send authenticated request to Quickbooks.
	 */
	public QbHttp(String credentialsName) {
		baseUrl = 'callout:' + credentialsName;
	}

	/**
	 * Creates and send an Http request: GET
	 * @param endpoint
	 */
	public String sendGet(String endpoint) {
		return sendHttp('GET', endpoint, getCommonHeaders(null), null);
	}

	/**
	 * Creates and send an Http request: GET
	 * @param endpoint
	 * @param headers
	 */
	public String sendGet(String endpoint, Map<String, String> headers) {
		return sendHttp('GET', endpoint, getCommonHeaders(headers), null);
	}

	/**
	 * Creates and send an Http request: POST
	 * @param endpoint
	 * @param body
	 * @return new HTTPResponse
	 */
	public String sendPost(String endpoint, QbJsonSerializable body) {
		return sendHttp('POST', endpoint, getCommonHeaders(null), body);
	}

	/**
	 * Creates and send an Http request: POST
	 * @param endpoint
	 * @param headers
	 * @param body
	 * @return new HTTPResponse
	 */
	public String sendPost(String endpoint,
			Map<String, String> headers, QbJsonSerializable body) {
		return sendHttp('POST', endpoint, getCommonHeaders(headers), body);
	}

	/**
	 * Creates and send an Http request: POST
	 * @param endpoint
	 * @param headers
	 * @param body
	 * @return new HTTPResponse
	 */
	public String sendPut(String endpoint,
			Map<String, String> headers, QbJsonSerializable body) {
		return sendHttp('PUT', endpoint, getCommonHeaders(headers), body);
	}

	/**
	 * Creates and send an Http request: POST, PUT, UPDATE
	 * @param method
	 * @param headers
	 * @param body
	 * @return new HTTPResponse
	 */
	private String sendHttp(String method, String endpoint,
			Map<String, String> headers, QbJsonSerializable body) {
		HTTPResponse resp;

		/* if (Test.isRunningTest()) {
			HTTPResponse resp = new HTTPResponse();
			resp.setStatusCode(mockupResponseCode);
			resp.setBody(mockupResponseBody);
			return resp;
		} else */	
		try {
			HttpRequest req;
			Http httpAction;

			req = new HttpRequest();
			req.setEndpoint(baseUrl + endpoint);

			if (headers != null)
				for (String nm : headers.keySet())
					req.setHeader(nm, headers.get(nm));

			req.setMethod(method);
			req.setTimeout(60000);

			if (body != null)
				req.setBody(body.serialize());

			httpAction = new Http();
			resp = httpAction.send(req);
		} catch (Exception ex) {
			throw new QbExceptions.HttpException(ex.getMessage());
		}

		if (resp.getStatusCode() >= 400)
			throw new QbExceptions.HttpException(
				'Error reported by Quickbooks during the current ' +
				'operation.\n' +
				resp.getStatusCode() + ': ' + 
				resp.getStatus() + '\n' +
				resp.getBody());

		return resp.getBody();
	}
	
	/**
	 * @param extraHeaders
	 * @return the basic header used i
	 * most autneticated requests
	 */
	private Map<String, String> getCommonHeaders(Map<String, String> extraHeaders) {
		Map<String, String> hs;
		hs = new Map<String, String> {
			'content-type' => 'application/json',
			'accept' => 'application/json'
		};

		if (extraHeaders != null)
			hs.putAll(extraHeaders);
		
		return hs;
	}
}
