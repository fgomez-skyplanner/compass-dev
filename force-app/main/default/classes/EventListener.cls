/**
 * Basic functionality for Compass' QUickbooks Event handler. 
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 7/7/2020
 */
public abstract class EventListener extends QbEventListener {
	private String businessType;
	private QuickbookAccount qAccount;

	/**
	 * @param qAccount
	 * @param businessType
	 */
	public EventListener(QuickbookAccount qAccount, String businessType) {
		super(qAccount.verificationToken);
		this.qAccount = qAccount;
		this.businessType = businessType;
	}

	/**
	 * Proces the received  events from QUickbooks.
	 * In this case, we create record of activities 
	 * in the oppotunity
	 */
	protected void processEvents() {
		Opportunity opp;
		QbEstimate estimate;
		QbInvoice invoice;
		QbPayment payment;
		QbConnector connector;
		String status, emailStatus;
		Decimal amount;

		// the connector is need to request info
		connector = new QbConnector(qAccount.namedCredential, qAccount.realmId);

		// depending on the operation, we need to extract
		// the estimate Id...
		// THE FORMAT SENT FROM QUICKBOOKS IS SHAPED
		// TO SEND A LIST OF EVENTS, BUT ONLY ONE EVENT IS SENT 
		// AT A TIME.
		for (QbEvent e : getEvents()) {
			switch on e.name {
				when 'Estimate' {
					// the opportunity contains the id
					// of the estimate created
					opp = getOpportunity(e.id);
					estimate = connector.getEstimate(e.id);
					status = estimate.TxnStatus;
					emailStatus = estimate.EmailStatus;
					amount = estimate.TotalAmt;
				}
				when 'Invoice' {
					// we need more info about this invoice...
					invoice = connector.getInvoice(e.id);
					status = null;
					emailStatus = invoice.EmailStatus;
					amount = invoice.TotalAmt;

					// the estimate id is in the LinkedTxn reference
					for (QbTxReference r : invoice.LinkedTxn)
						if (r.TxnType == 'Estimate') {
							opp = getOpportunity(r.TxnId);
							break;
						}
				}
				when 'Payment' {
					// to find the estimate id,
					// we need the payment from QB
					payment = connector.getPayment(e.id);
					status = null;
					emailStatus = null;
					amount = payment.TotalAmt;

					// the payment has a link to the invoice
					// in the TxnId field, which is inside one of the lines.
					// we try to find the link to the invoice
					if (payment.Line != null && !payment.Line.isEmpty())
						for (QbTxReference r : payment.Line[0].LinkedTxn)
							if (r.TxnType == 'Invoice') {
								// we have the invoice id, we need more
								// info about this invoice to get the estimate id
								invoice = connector.getInvoice(r.TxnId);

								// the estimate id is in the LinkedTxn reference
								for (QbTxReference r1 : invoice.LinkedTxn)
									if (r1.TxnType == 'Estimate') {
										opp = getOpportunity(r1.TxnId);
										break;
									}

								break;
							}
				}
				when else {
					// we do nothing here at this point...
				}
			}

			// if an opportunity was found, then we need to
			// create an activity records
			if (opp != null)
				insert new QuickbooksActivity__c(
					Operation__c = e.operation,
					QbId__c = e.id,
					QbName__c = e.name,
					QbStatus__c = status,
					QbEmailStatus__c = emailStatus,
					QbAmount__c = amount,
					Opportunity__c = opp.Id
				);
		}
	}

	/**
	 * @param estimateId
	 * @return the current opportunity which needs to contains
	 * the proper business type, and the estimate Id
	 */
	private Opportunity getOpportunity(String estimateId) {
		try {
			return [
				SELECT Id
				FROM Opportunity
				WHERE RecordType.DeveloperName IN :qAccount.listenerSharedtypes
				AND QbEstimateId__c = :estimateId
				LIMIT 1
			];
		} catch (QueryException ex) {
			return null;
		}
	}
}
