/**
 * Miscellaneous method to help in the process.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 6/19/2020
 */
public class QbUtility {
	
	/**
	 * Given a string with placeholders in it, replaces
	 * the placeholders with actuals values from the value
	 * map. The placeholder-value match is achieved by having
	 * the placeholder under the same Key, in the placeholder
	 * map, and its correspondent value in the valueMap.
	 * @param originalValue
	 * @param placeholderMap
	 * @param valueMap
	 */
	public static String replacePlaceholders(String originalValue,
			Map<String, String> placeholderMap, Map<String, String> valueMap) {
		String result = originalValue + '';

		for (String key : valueMap.keySet())
			result = result.replace(placeholderMap.get(key),
				EncodingUtil.urlEncode(valueMap.get(key), 'UTF-8'));
		
		return result;
	}
}
