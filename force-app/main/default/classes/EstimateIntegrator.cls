/**
 * Connects Compass contact with Quickbook's customer.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 6/24/2020
 */
public class EstimateIntegrator extends Integrator {

	/**
	 * @param credentialsName
	 * @param realmId
	 */
	public EstimateIntegrator(String credentialsName, String realmId) {
		super(credentialsName, realmId);
	}
	
	/**
	 * Incable method that allows creating an estimate on
	 * Quickbooks from flows and process.
	 * @param opportunityInfos
	 */
	@InvocableMethod(label='Create Estimate on Quickbooks')
	public static void saveToQuickbooks(List<IntegrationInfo> opportunityInfos) {
		// we transefer the interaction to a future method
		// so any callout do not interfere with DML operation...
		for (IntegrationInfo ci : opportunityInfos)
			saveToQuickbooksAsync(ci.credentialsName, ci.realmId, ci.recordId);
	}

	/**
	 * @param credentialsName
	 * @param realmId
	 * @param opportunityId
	 */
	@Future(callout=true)
	public static void saveToQuickbooksAsync(
			String credentialsName, String realmId, Id opportunityId) {
		EstimateIntegrator integrator;
		
		// the integrator will take care of the communication
		integrator = new EstimateIntegrator(credentialsName, realmId);
		integrator.saveToQuickbooks(opportunityId);
	}

	/**
	 * @param opportunityId
	 */
	private void saveToQuickbooks(Id opportunityId) {
		Opportunity opp;
		Contact primaryContact;
		QbAddress billingAddress, shippingAddress;
		QbEstimate estimate;
		QbItemLine lineItem;
		QbSalesItemLineDetail salesItem;
		Decimal totalPrice;

		// we need the oop
		opp = getOpportunity(opportunityId);
		primaryContact = getPrimaryContact(opp.AccountId);

		// we make sure info is correct
		validateForEstimate(opp, primaryContact);

		// billing and shipping addresses come from the related account
		billingAddress = getBillingAddressFromAccount(opp.Account);
		shippingAddress = getShippingAddressFromAccount(opp.Account);

		// the only line item is created from the defaul product
		salesItem = new QbSalesItemLineDetail(null,
			new QbReference(opp.QbProductName__c, opp.QbProductId__c),
				opp.QbProductQuantity__c, opp.QbProductUnitPrice__c);

		// the total amount is simply the unit price plus the quantity
		totalPrice = salesItem.Qty * salesItem.UnitPrice;

		// we are readya to create the estimate
		estimate = new QbEstimate(opp.QbEstimateId__c,
			opp.QbEstimateSyncToken__c, null, null, totalPrice,
			new QbEmail(primaryContact.Email),
			billingAddress, shippingAddress,
			new QbReference(opp.Account.Name, opp.Account.QbId__c),
			new QbReference(null, opp.RecordType.DeveloperName));

		// we now create the only line
		lineItem = new QbItemLine('SalesItemLineDetail', totalPrice,
			opp.QbProductName__c, 1, salesItem, null);
		estimate.addLine(lineItem);

		// now we are ready to send it to quickbooks...
		// we update it if it exists
		if (estimate.exists())
			estimate = connector.updateEstimate(estimate);
		// if it does not exists yet, we create and save the resulting
		// id and tokens to the opportunity
		else {
			estimate = connector.createEstimate(estimate);
			opp.QbEstimateId__c = estimate.Id;
		}
		
		opp.QbEstimateSyncToken__c = estimate.SyncToken;
		update opp;
	}

	/**
	 * @param opportunityId
	 * @return the opportunity with the specified id
	 */
	private Opportunity getOpportunity(Id opportunityId) {
		return [
			SELECT Id,
				QbEstimateId__c,
				QbEstimateSyncToken__c,
				QbProductId__c,
				QbProductName__c,
				QbProductQuantity__c,
				QbProductUnitPrice__c,
				RecordType.DeveloperName,
				AccountId,
				Account.Name,
				Account.BillingState,
				Account.BillingCity,
				Account.BillingPostalCode,
				Account.BillingStreet,
				Account.BillingCountry,
				Account.ShippingState,
				Account.ShippingCity,
				Account.ShippingPostalCode,
				Account.ShippingStreet,
				Account.ShippingCountry,
				Account.QbId__c
			FROM Opportunity
			WHERE Id = :opportunityId
		];
	}

	/**
	 * @param accountId
	 * @return the primary contact of the related account
	 */
	private Contact getPrimaryContact(Id accountId) {
		try {
			return [
				SELECT Id,
					Email
				FROM Contact
				WHERE AccountId = :accountId
				AND Is_Primary_Contact__c = true
				LIMIT 1
			];
		} catch (QueryException ex) {
			return null;
		} 
	}

	/**
	 * Validates is the infor is ready to generate an estimate
	 * @param opp
	 * @param primaryContact
	 */
	private void validateForEstimate(Opportunity opp, Contact primaryContact) {
		// we need a quickbooks client
		if (String.isBlank(opp.Account.QbId__c))
			throw new IntegratorException('The custom has not been syncronized ' +
				'with the Quickbooks system. An estimate cannot be created.');

		// we cannot procees if there is no primary contact
		if (primaryContact == null)
			throw new IntegratorException('No Primary Contact has be established ' +
				'for the related Account. An estimate cannot be created.');

		// let's make sure all fields are there
		if (String.isBlank(opp.QbProductId__c) ||
				opp.QbProductQuantity__c == null ||
				opp.QbProductUnitPrice__c == null)
			throw new IntegratorException('Some or all of the information ' +
				'about the Default Product is missing. Please, setup the '+
				'Default Product again.');
	}
}
