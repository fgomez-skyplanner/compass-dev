/**
 * Represents an event sent from Quickbook about a change
 * in the quickbooks system.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 7/7/2020
 */
public class QbEvent {
	public String name { get; set; }
	public String id { get; set; }
	public String operation { get; set; }
	public String lastUpdated { get; set; }
	public String deletedId { get; set; }
}