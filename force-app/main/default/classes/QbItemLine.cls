/**
 * Represents a quickbook estimate line item.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 6/18/2020
 */
public class QbItemLine {
	public String DetailType { get; set; }
	public Decimal Amount { get; set; }

	public String Description { get; set; }
	public Integer LineNum { get; set; }

	public QbSalesItemLineDetail SalesItemLineDetail { get; set; }
	public QbDiscountLineDetail DiscountLineDetail { get; set; }

	public List<QbTxReference> LinkedTxn { get; set; }

	/**
	 * Main constructor
	 */
	public QbItemLine(String DetailType, Decimal Amount, 
			String Description, Integer LineNum,
			QbSalesItemLineDetail SalesItemLineDetail,
			QbDiscountLineDetail DiscountLineDetail) {
		this.DetailType = DetailType;
		this.Amount = Amount;
		this.Description = Description;
		this.LineNum = LineNum;
		this.SalesItemLineDetail = SalesItemLineDetail;
		this.DiscountLineDetail = DiscountLineDetail;
	}
}
