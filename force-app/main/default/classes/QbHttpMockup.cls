/**
 * Mocks up an http request.
 * @author Fernando Gomez
 */
@isTest
public class QbHttpMockup implements HttpCalloutMock {
	private Integer statusCode;
	private String body;
	Map<String, String> headers;

	/**
	 * Main Constructor
	 * @param statusCode
	 */
	public QbHttpMockup(Integer statusCode, 
			Map<String, String> headers, String body) {
		this.statusCode = statusCode;
		this.headers = headers;
		this.body = body;
	}

	/**
	 * Will be called when there is a
	 * http request in test method if the class
	 * is set as mockup
	 * @param req
	 * @return HTTPResponse
	 */
	public HTTPResponse respond(HTTPRequest req) {
		HTTPResponse res;
		
		// Create a fake response.
		res = new HTTPResponse();
		res.setStatusCode(statusCode);
		res.setBody(body);

		return res;
	}
}
