/**
 * Represents a quickbook invoice.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 6/18/2020
 */
public class QbInvoice extends QbJsonSerializable {
	public String TxnDate { get; set; }
	public String domain { get; set; }
	public String PrintStatus { get; set; }
	public String EmailStatus { get; set; }
	public Decimal TotalAmt { get; set; }
	public String DueDate { get; set; }
	public String ApplyTaxAfterDiscount { get; set; }
	public String DocNumber { get; set; }
	public QbReference SalesTermRef { get; set; }
	public QbReference CustomerMemo { get; set; }
	public Decimal Deposit { get; set; }
	public Decimal Balance { get; set; }
	public QbReference CustomerRef { get; set; }
	public QbEmail BillEmail { get; set; }
	public QbAddress ShipAddr { get; set; }
	public QbAddress BillAddr { get; set; }

	public List<QbItemLine> Line { get; set; }
	public List<QbTxReference> LinkedTxn { get; set; }

	/**
	 * @param jsonStr
	 * @return a deserialized version
	 */
	public static QbInvoice deserializeMap(String jsonStr) {
		return 
			// the map brings the Invoice under the Invoice
			// property, and a time property with a date span,
			// sp we have to parse to a middle object
			// and the convert the contents of Invoice 
			// which will contain the actual Invoice
			((QbInvoiceResponse)JSON.deserialize(
				jsonStr, QbInvoiceResponse.class)).Invoice;
	}

	/**
	 * This class is just for conversion.
	 * The time stamp is not used outside.
	 */
	private class QbInvoiceResponse {
		QbInvoice Invoice { get; set; }
	}
}
