/**
 * Represents a quickbook customer.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 6/18/2020
 */
public class QbItem extends QbJsonSerializable {
	@AuraEnabled
	public String FullyQualifiedName { get; set; }

	public String Name { get; set; }
	public Boolean TrackQtyOnHand { get; set; }
	public String Type { get; set; }
	public QbReference IncomeAccountRef { get; set; }
	public Boolean Taxable { get; set; }
	public Boolean Active { get; set; }

	@AuraEnabled
	public Decimal UnitPrice { get; set; }
	
	@AuraEnabled
	public String Description { get; set; }

	/**
	 * @param jsonStr
	 * @return a deserialized version
	 */
	public static QbItem deserialize(String jsonStr) {
		return (QbItem)JSON.deserialize(jsonStr, QbItem.class);
	}

	/**
	 * @param jsonStr
	 * @return a deserialized version
	 */
	public static QbItem deserializeMap(String jsonStr) {
		return ((QbItemResponse)JSON.deserialize(
			jsonStr, QbItemResponse.class)).Item;
	}

	/**
	 * This class is just for conversion.
	 * The time stamp is not used outside.
	 */
	private class QbItemResponse {
		QbItem Item { get; set; }
	}
}
