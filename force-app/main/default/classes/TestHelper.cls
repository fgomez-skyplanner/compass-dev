/**
 * Helper class for test methods
 * @author fernando Gomez, SkyPlanner LLC
 */
@isTest
public class TestHelper {
	
	/**
	 * @return a Master account
	 */
	public static Account getMasterAccount() {
		return new Account(
			Name = 'Sample Master Account',
			RecordTypeId = [
				SELECT Id
				FROM RecordType
				WHERE SobjectType = 'Account'
				AND DeveloperName = 'Master_Account'
			].Id
		);
	}

	/**
	 * @param parentId
	 * @return a solar business account
	 */
	public static Account getSolarAccount(Id parentId) {
		return new Account(
			Name = 'Sample Solar Account',
			Business_Type__c = 'Solar',
			ParentId = parentId,
			BillingCountry = 'US',
			RecordTypeId = [
				SELECT Id
				FROM RecordType
				WHERE SobjectType = 'Account'
				AND DeveloperName = 'Business_Unit_Account'
			].Id
		);
	}
	
	/**
	 * @param parentId
	 * @return a solar business account
	 */
	public static Account getPowerAccount(Id parentId) {
		return new Account(
			Name = 'Sample Power Account',
			Business_Type__c = 'Power',
			ParentId = parentId,
			BillingStreet = '123 NW 23st',
			BillingCity = 'Miami',
			BillingState = 'FL',
			BillingPostalCode = '33193',
			BillingCountry = 'US',
			RecordTypeId = [
				SELECT Id
				FROM RecordType
				WHERE SobjectType = 'Account'
				AND DeveloperName = 'Business_Unit_Account'
			].Id
		);
	}
	
	/**
	 * @param accountId
	 * @return a primary contact
	 */
	public static Contact getPrimaryContact(Id accountId) {
		return new Contact(
			FirstName = 'John',
			LastName = 'Doe',
			Email = 'johndoe@email.com',
			Is_Primary_Contact__c = true,
			AccountId = accountId,
			Phone = '(277) 441-3456'
		);
	}

	/**
	 * @param accountId
	 * @return a solar Opportunity
	 */
	public static Opportunity getSolarOpportunity(Id accountId) {
		return new Opportunity(
			Name = 'Sample Solar Opportunity',
			AccountId = accountId,
			CloseDate = System.today(),
			StageName = 'Lead Origination',
			RecordTypeId = [
				SELECT Id
				FROM RecordType
				WHERE SobjectType = 'Opportunity'
				AND DeveloperName = 'Solar'
			].Id
		);
	}
}
