({
	handleKeyUp: function(component, event, helper) {
		var action, errors, input, record, state, results;

		// we remove any error
		component.set("v.isError", false);
		component.set("v.isSuccess", false);
		component.set("v.hasResults", false);
		component.set("v.selected", null);

		if (event.keyCode === 13) {
			component.set("v.isSearching", true);
			component.find("searchCriteria")
			component.set("v.results", null);

			input = component.find("searchCriteria");
			input.set("v.disabled", true);

			// we call the server and request products
			action = component.get("c.searchProducts");

			// attributes
			record = component.get("v.simpleRecord");
			action.setParams({
				"businessType": record.RecordType.DeveloperName,
				"criteria": input.get("v.value")
			});

			// Add callback behavior for when response is received
			action.setCallback(this, function(response) {
				state = response.getState();
				
				if (state === "SUCCESS") {
					results = response.getReturnValue();
					component.set("v.isSuccess", true);
					component.set("v.results", results);
					component.set("v.hasResults",
						results != null && results.length > 0);
					component.set("v.isSearching", false);
					input.set("v.disabled", false);
				} else {
					component.set("v.isError", true);
					component.set("v.isSearching", false);
					input.set("v.disabled", false);

					errors = response.getError();
					if (errors[0] && errors[0].message)
						component.set("v.errMessage", errors[0].message);
				}
			});
			// Send action off to be executed
			$A.enqueueAction(action);
		}
	},
	handleSelect: function(component, event, helper) {
		var results = component.get("v.results"),
			index = event.currentTarget.dataset.index,
			selected = component.get("v.selected"),
			current = results[index];

		if (selected == null || selected.Id != current.Id) {
			// component.set("v.selectedQty", null);
			component.set("v.selected", current);
		}
	},
	handleChoose: function(component, event, helper) {
		var action, errors, record, state,
			selected = component.get("v.selected"),
			qty = component.get("v.selectedQty");

		component.set("v.isError", false);
		component.set("v.isLoading", true);

		// we call the server and request products
		action = component.get("c.saveDefaultProduct");

		// attributes
		record = component.get("v.simpleRecord");
		action.setParams({
			"businessType": record.RecordType.DeveloperName,
			"opportunityId": record.Id,
			"product": selected,
			"qty": qty
		});

		// Add callback behavior for when response is received
		action.setCallback(this, function(response) {
			state = response.getState();
			console.log(state);
			
			if (state === "SUCCESS") {
				// we simply close the modal since we are done
				component.set("v.isSuccess", false);
				component.set("v.isLoading", false);
				component.set("v.isCompleted", true);
				// we wait a second or two, and close any modal
				setTimeout(function() {
					component.find("overlayLib").notifyClose();
				}, 1000);
			} else {
				// or we show the error that happened
				component.set("v.isError", true);
				component.set("v.isLoading", false);

				errors = response.getError();
				if (errors[0] && errors[0].message)
					component.set("v.errMessage", errors[0].message);
			}
		});
		// Send action off to be executed
		$A.enqueueAction(action);
	}
})
